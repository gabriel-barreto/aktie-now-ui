module.exports = {
  stories: ['../stories/**/*.stories.(js|ts|mdx)'],
  addons: [
    '@storybook/preset-create-react-app',

    '@storybook/addon-actions',
    '@storybook/addon-knobs',
    '@storybook/addon-links',
    {
      name: '@storybook/addon-docs',
      options: { configureJSX: true },
    },
    {
      name: '@storybook/addon-storysource',
      options: {
        loaderOptions: {
          prettierConfig: { printWidth: 80, singleQuote: false },
        },
      },
    },
  ],
};
