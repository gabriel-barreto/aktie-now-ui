const { NODE_ENV: env } = process.env;

const headers = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};

const config = {
  development: {
    baseURL: 'http://localhost:5000/api',
    headers,
  },
  production: {
    baseURL: 'https://aktie-now-teste-api.herokuapp.com/api',
    headers,
  },
};

const target = env === 'production' ? env : 'development';
export default config[target];
