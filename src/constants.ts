export const failToFetch =
  'Ocorreu um erro ao tentar recuperar as informações do(s) livro(s) e não consegui encontrá-las. Por favor, aguarde alguns instantes e tente novamente.';

export const successRemove =
  'As informações do livro foram removidas com sucesso e não estão mais disponíveis!';
export const failToRemove =
  'Ocorreu um erro ao tentar remover as informações do livro. Por favor, aguarde alguns instantes e tente novamente!';

export const successCreation =
  'As informações do livro foram registradas com sucesso.';
export const failToCreate =
  'Ocorreu um erro ao tentar registrar as informações do livro. Por favor, verifique as informações, aguarde alguns instantes e tente novamente.';

export const successUpdate =
  'As informações do livro foram atualizadas com sucesso!';
export const failToUpdate =
  'Ocorreu um erro ao tentar atualizar as informações do livro. Por favor, verifique as informações, aguarde alguns instantes e tente novamente.';
