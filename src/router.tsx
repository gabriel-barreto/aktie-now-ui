import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import DetailsBookPage from './pages/Books/Details';
import ListBooksPage from './pages/Books/List';
import NewBookPage from './pages/Books/New';
import UpdateBookPage from './pages/Books/Update';

export default function Router(): JSX.Element {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/livros/novo" component={NewBookPage} />

        <Route path="/livros/:id/atualizar" component={UpdateBookPage} />
        <Route path="/livros/:id" component={DetailsBookPage} exact />

        <Route path="/livros" component={ListBooksPage} exact />

        <Redirect from="/" to="/livros" />
      </Switch>
    </BrowserRouter>
  );
}
