import React from 'react';

import { Trash } from 'styled-icons/boxicons-solid';
import { Edit } from 'styled-icons/material';
import { ViewShow } from 'styled-icons/zondicons';

import { Book } from '../../../../types';

import * as S from './styled';

type Props = {
  rows: Book[];
  onView: (_id: string) => void;
  onUpdate: (_id: string) => void;
  onRemove: (_id: string) => void;
};
export default function BooksTable({
  rows,
  onView,
  onUpdate,
  onRemove,
}: Props): JSX.Element {
  return (
    <S.BooksTableWrapper>
      <S.BooksTable>
        <S.Header>
          <S.Row>
            <S.HeaderItem>Título</S.HeaderItem>
            <S.HeaderItem>Autor(es)</S.HeaderItem>
            <S.HeaderItem>Avaliações</S.HeaderItem>
            <S.HeaderItem>Última Atualização</S.HeaderItem>
            <S.HeaderItem>Ações</S.HeaderItem>
          </S.Row>
        </S.Header>
        <S.Body>
          {rows.map(({ _id, title, authors, rating, updatedAt }) => (
            <S.Row key={_id}>
              <S.Cell>{title}</S.Cell>
              <S.Cell>{authors}</S.Cell>
              <S.Cell>{rating}</S.Cell>
              <S.Cell>{updatedAt}</S.Cell>
              <S.Cell>
                <S.ActionsContainer>
                  <S.Action
                    type="button"
                    title="Ver detalhes"
                    onClick={() => onView(_id)}
                  >
                    <ViewShow size={20} />
                  </S.Action>
                  <S.Action
                    type="button"
                    title="Editar"
                    onClick={() => onUpdate(_id)}
                  >
                    <Edit size={20} />
                  </S.Action>
                  <S.Action
                    type="button"
                    title="Remover"
                    onClick={() => onRemove(_id)}
                  >
                    <Trash size={20} />
                  </S.Action>
                </S.ActionsContainer>
              </S.Cell>
            </S.Row>
          ))}
        </S.Body>
      </S.BooksTable>
    </S.BooksTableWrapper>
  );
}
