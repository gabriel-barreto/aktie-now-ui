import styled from 'styled-components';

export const BooksTableWrapper = styled.div`
  overflow-x: auto;
  @media (min-width: 1024px) {
    width: 100%;
  }
`;

export const BooksTable = styled.table`
  @media (min-width: 1024px) {
    width: 100%;
  }
`;

export const HeaderItem = styled.th`
  font-size: 1.4rem;
  text-align: left;
  padding: 0.8rem 1.6rem;
  white-space: nowrap;

  :first-of-type {
    padding-left: 0rem;
  }

  @media (min-width: 1024px) {
    padding: 0.8rem;
    :first-of-type {
      padding-left: 0.8rem;
    }
  }
`;

export const Row = styled.tr``;

export const Cell = styled.td`
  color: ${({ theme }) => theme.colors.dark.hex()};
  font-size: 1.6rem;
  padding: 1.6rem;
  white-space: nowrap;

  @media (min-width: 1024px) {
    padding: 1.6rem 0.8rem;
  }

  :first-of-type {
    padding-left: 0.8rem;
  }
`;

export const Action = styled.button`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.primary.hex()};
  border: none;
  display: flex;
  height: 4.4rem;
  justify-content: center;
  padding: 0;
  transition: background-color 400ms;
  width: 4.4rem;
  will-change: background-color;

  > svg {
    fill: ${({ theme }) => theme.colors.lightest.hex()};
    transition: fill 400ms;
    will-change: fill;
  }

  :hover {
    background-color: transparent;
    > svg {
      fill: ${({ theme }) => theme.colors.primaryDark.hex()};
    }
  }
`;

export const ActionsContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-end;
  width: fit-content;
  > ${Action}:not(:last-of-type) {
    margin: 0 0.4rem 0 0;
  }
`;

export const Header = styled.thead``;

export const Body = styled.tbody`
  > ${Row} {
    border-bottom: 1px solid ${({ theme }) => theme.colors.dark.alpha(0.16)};
    transition: background-color 400ms;
    will-change: background-color;
    :hover {
      background-color: ${({ theme }) => theme.colors.light.hex()};
    }
  }
`;
