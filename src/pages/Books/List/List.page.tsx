import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

import { Add } from 'styled-icons/material';

import { Layout, Message } from '../../../components';
import * as content from '../../../constants';
import { $books } from '../../../services';
import { Book } from '../../../types';

import DeletionConfirmation from '../DeletionConfirmation';
import BooksTable from './BooksTable';

export default function ListBooksPage(): JSX.Element {
  const history = useHistory();
  const [books, setBooks] = useState<Book[]>([]);
  const [toRemove, setToRemove] = useState('');

  function onBookDetails(_id: string): void {
    history.push(`/livros/${_id}`);
  }
  function onBookUpdate(_id: string): void {
    history.push(`/livros/${_id}/atualizar`);
  }
  function onBookRemove(_id: string): void {
    setToRemove(_id);
  }

  function removeBook(_id: string): void {
    setToRemove('');
    $books
      .remove(toRemove)
      .then(() => {
        toast.success(content.successRemove);

        const index = books.findIndex(({ _id: bookId }) => bookId === toRemove);
        const updatedBooks = [...books];
        updatedBooks.splice(index, 1);
        setBooks(updatedBooks);
      })
      .catch(() => {
        toast.error(content.failToRemove);
      });
  }

  useEffect(() => {
    $books
      .fetchAll()
      .then(setBooks)
      .catch((_) => toast.error(content.failToFetch));
  }, []);

  const LayoutRightActions = [
    {
      Icon: <Add size={24} />,
      label: 'Novo Livro',
      onClick: () => history.push('/livros/novo'),
    },
  ];

  return (
    <Layout title="Todos os livros" actions={{ right: LayoutRightActions }}>
      <>
        {books.length ? (
          <BooksTable
            rows={books}
            onView={onBookDetails}
            onUpdate={onBookUpdate}
            onRemove={onBookRemove}
          />
        ) : (
          <Message content="Nenhum livro encontrado!" />
        )}
        <DeletionConfirmation
          _id={toRemove}
          onClose={() => setToRemove('')}
          onConfirm={removeBook}
        />
      </>
    </Layout>
  );
}
