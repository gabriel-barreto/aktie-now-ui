import styled from 'styled-components';
import { Form as UnformForm } from '@unform/web';

import { Input as RawInput } from '../../../components';

export const Form = styled(UnformForm)`
  padding: 0.8rem 0 0 0;
  margin: 0 auto;
  max-width: 40rem;
  width: 100%;
`;

export const Input = styled(RawInput)`
  :not(:last-of-type) {
    margin: 0 0 2.4rem 0;
  }
`;

export const Note = styled.p`
  color: ${({ theme }) => theme.colors.dark.alpha(0.72)};
  font-size: ${({ theme }) => theme.sizes.light};
`;

export const Footer = styled.div`
  margin: 1.6rem 0 0 0;
  > ${Note} {
    margin: 0 0 1rem 0;
  }
`;
