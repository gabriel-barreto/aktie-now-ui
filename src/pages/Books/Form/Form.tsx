import { FormHandles } from '@unform/core';
import React, { useRef } from 'react';
import { toast } from 'react-toastify';

import { failToCreate } from '../../../constants';
import { Button } from '../../../components';
import { $books } from '../../../services';
import { BookFormData, BookFormRawData } from '../../../types';

import * as S from './styled';

type Props = {
  className?: string;
  data?: BookFormData;
  submitLabel?: string;
  onSubmit: (data: BookFormData) => void;
};

export default function BookForm({
  className = '',
  data,
  submitLabel = 'Adicionar Livro',
  onSubmit,
}: Props): JSX.Element {
  const formRef = useRef<FormHandles>(null);

  async function handleSubmit(formData: BookFormRawData) {
    try {
      const payload = await $books.handleFormSubmit(formData, formRef);
      return onSubmit(payload);
    } catch (ex) {
      toast.error(failToCreate);
    }
    return null;
  }

  return (
    <S.Form className={className} ref={formRef} onSubmit={handleSubmit}>
      <S.Input label="Título:" name="title" value={data?.title} required />
      <S.Input
        label="Autor(es):"
        name="authors"
        helper="Separe os valores por vírgulas."
        value={data?.authors?.join(', ')}
        required
      />
      <S.Input
        label="Gêneros:"
        name="categories"
        helper="Separe os valores por vírgulas."
        value={data?.categories?.join(', ')}
        required
      />
      <S.Input
        label="Editora:"
        name="publisher"
        value={data?.publisher}
        required
      />
      <S.Footer>
        <S.Note>* Campos Requeridos</S.Note>
        <Button type="submit" label={submitLabel} />
      </S.Footer>
    </S.Form>
  );
}
