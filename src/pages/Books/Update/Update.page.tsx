import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { useHistory, useParams } from 'react-router-dom';

import { LeftArrowAlt } from 'styled-icons/boxicons-regular';

import * as constants from '../../../constants';
import { Layout } from '../../../components';
import { $books } from '../../../services';
import { BookFormData } from '../../../types';

import Form from '../Form';

export default function UpdateBookPage(): JSX.Element {
  const history = useHistory();
  const { id } = useParams();
  const [book, setBook] = useState<BookFormData>({} as BookFormData);

  useEffect(() => {
    $books
      .fetchOne(id)
      .then((payload) => {
        setBook((prev) => ({
          ...prev,
          ...payload,
        }));
      })
      .catch(() => toast.error(constants.failToFetch));
  }, []);

  async function updateBook(payload: BookFormData) {
    $books
      .update(id, payload)
      .then((updated) => {
        setBook((prev) => ({
          ...prev,
          ...updated,
        }));
        toast.success(constants.successUpdate);
      })
      .catch(() => toast.error(constants.failToUpdate));
  }

  const LeftActions = [
    {
      Icon: <LeftArrowAlt size={20} />,
      label: 'Voltar',
      onClick: () => history.go(-1),
    },
  ];
  return (
    <Layout
      title={book?.title ? `Atualizar - ${book.title}` : 'Atualizar Livro'}
      actions={{ left: LeftActions }}
    >
      <>
        {book ? (
          <Form
            data={book}
            submitLabel="Atualizar Livro"
            onSubmit={updateBook}
          />
        ) : null}
      </>
    </Layout>
  );
}
