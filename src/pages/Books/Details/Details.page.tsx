import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

import { Edit } from 'styled-icons/material';
import { LeftArrowAlt } from 'styled-icons/boxicons-regular';
import { Trash } from 'styled-icons/boxicons-solid';

import { $books } from '../../../services';
import { Layout, Message } from '../../../components';
import { BookDetails } from '../../../types';
import * as content from '../../../constants';

import DeletionConfirmation from '../DeletionConfirmation';

import BookInfo from './Book/Book';

export default function DetailsBookPage(): JSX.Element {
  const history = useHistory();
  const { id } = useParams();
  const [toRemove, setToRemove] = useState('');
  const [book, setBook] = useState<BookDetails | null>(null);

  function onRemove(): void {
    setToRemove(id);
  }

  function removeBook(_id: string): void {
    $books
      .remove(_id)
      .then(() => toast.success(content.successRemove))
      .catch(() => toast.error(content.failToRemove))
      .finally(() => {
        setToRemove('');
      });
  }

  useEffect(() => {
    $books
      .fetchOne(id)
      .then(setBook)
      .catch(() => toast.error(content.failToFetch));
  }, []);

  const LeftActions = [
    {
      Icon: <LeftArrowAlt size={24} />,
      label: 'Voltar',
      onClick: () => history.go(-1),
    },
  ];
  const RightActions = [
    {
      Icon: <Edit size={24} />,
      label: 'Editar',
      onClick: () => history.go(-1),
    },
    {
      Icon: <Trash size={24} />,
      label: 'Remover',
      onClick: () => onRemove(),
    },
  ];

  return (
    <Layout
      title={book?.title ? book.title : 'Detalhes do Livro'}
      actions={{ left: LeftActions, right: RightActions }}
    >
      <>
        {book ? (
          <BookInfo {...book} />
        ) : (
          <Message content="Livro não encontrado!" />
        )}
        <DeletionConfirmation
          _id={toRemove}
          onClose={() => setToRemove('')}
          onConfirm={removeBook}
        />
      </>
    </Layout>
  );
}
