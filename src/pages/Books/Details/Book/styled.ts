import styled from 'styled-components';

export const Book = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  margin: -7.2rem auto 0 auto;
  max-width: 40rem;
  width: 100%;
`;

export const Icon = styled.span`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.light.hex()};
  display: flex;
  border-radius: 50%;
  box-shadow: ${({ theme }) => theme.shadows.md};
  height: 8rem;
  justify-content: center;
  margin: 0 0 1.6rem 0;
  width: 8rem;
`;

export const Prop = styled.div`
  width: 100%;
  :not(:last-of-type) {
    margin: 0 0 2.4rem 0;
  }
`;

export const Value = styled.p`
  background-color: ${({ theme }) => theme.colors.light.hex()};
  color: ${({ theme }) => theme.colors.darkest.hex()};
  font-size: ${({ theme }) => theme.sizes.h3};
  line-height: 120%;
  padding: 1.6rem;
`;

export const Key = styled.p`
  color: ${({ theme }) => theme.colors.dark.alpha(0.8)};
  font-size: ${({ theme }) => theme.sizes.light};
  line-height: 120%;
  margin: 0 0 0.4rem 0;
`;
