import React from 'react';

import { Book as BookIcon } from 'styled-icons/icomoon';

import { BookDetails } from '../../../../types';

import * as S from './styled';

export default function Book({
  _id,
  title,
  authors,
  categories,
  rating,
  publisher,
  createdAt,
  updatedAt,
}: BookDetails): JSX.Element {
  return (
    <S.Book>
      <S.Icon>
        <BookIcon size={40} />
      </S.Icon>
      <S.Prop>
        <S.Key>Título:</S.Key>
        <S.Value>{title}</S.Value>
      </S.Prop>
      <S.Prop>
        <S.Key>Autor(es):</S.Key>
        <S.Value>{authors.join(', ')}</S.Value>
      </S.Prop>
      <S.Prop>
        <S.Key>Categorias:</S.Key>
        <S.Value>{categories.join(', ')}</S.Value>
      </S.Prop>
      <S.Prop>
        <S.Key>Média de Avaliações:</S.Key>
        <S.Value>{`${rating}/5.0`}</S.Value>
      </S.Prop>
      <S.Prop>
        <S.Key>Editora:</S.Key>
        <S.Value>{publisher}</S.Value>
      </S.Prop>
      <S.Prop>
        <S.Key>Última Atualização:</S.Key>
        <S.Value>{updatedAt}</S.Value>
      </S.Prop>
      <S.Prop>
        <S.Key>Registrado em:</S.Key>
        <S.Value>{createdAt}</S.Value>
      </S.Prop>
    </S.Book>
  );
}
