import React from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

import { LeftArrowAlt } from 'styled-icons/boxicons-regular';

import { Layout } from '../../../components';
import * as constants from '../../../constants';
import { $books } from '../../../services';
import { BookFormData } from '../../../types';

import Form from '../Form';

export default function NewBookPage(): JSX.Element {
  const history = useHistory();

  const LeftActions = [
    {
      Icon: <LeftArrowAlt size={24} />,
      label: 'Voltar',
      onClick: () => history.go(-1),
    },
  ];

  async function createBook(payload: BookFormData) {
    return $books
      .create(payload)
      .then(() => toast.success(constants.successCreation))
      .catch(() => toast.error(constants.failToCreate));
  }

  return (
    <Layout title="Novo Livro" actions={{ left: LeftActions }}>
      <Form onSubmit={createBook} />
    </Layout>
  );
}
