import React, { useState, useEffect } from 'react';

import { Modal } from '../../../components';

type Props = {
  _id?: string;
  onClose: () => void;
  onConfirm: (_id: string) => void;
};
export default function DeletionConfirmation({
  _id = '',
  onClose,
  onConfirm,
}: Props): JSX.Element {
  const [modalConfig, setModalConfig] = useState({
    state: false,
    title: 'Atenção, tem certeza que deseja prosseguir?',
    message:
      'Os dados do livro afetado por esta ação que está realizando não poderão ser recuperados futuramente.',
    action: {
      cancel: 'Manter',
      confirm: 'Prosseguir e Excluir',
    },
  });

  useEffect(() => {
    if (_id) {
      setModalConfig((prev) => ({ ...prev, state: true }));
    }
  }, [_id]);

  function onModalConfirm() {
    setModalConfig((prev) => ({ ...prev, state: false }));
    return onConfirm(_id);
  }

  function onModalClose() {
    setModalConfig((prev) => ({ ...prev, state: false }));
    return onClose();
  }

  return (
    <Modal {...modalConfig} onConfirm={onModalConfirm} onClose={onModalClose} />
  );
}
