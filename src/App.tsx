import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';

import Router from './router';
import AppContext from './contexts';
import { GlobalStyle, Theme } from './styles';

function App(): JSX.Element {
  return (
    <BrowserRouter>
      <ThemeProvider theme={Theme}>
        <AppContext>
          <>
            <GlobalStyle />
            <Router />
          </>
        </AppContext>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
