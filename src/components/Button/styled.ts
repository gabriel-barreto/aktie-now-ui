import styled from 'styled-components';

export const Icon = styled.span`
  display: block;
  margin: 0 0.4rem 0 0;

  > svg {
    fill: ${({ theme }) => theme.colors.lightest.hex()};
    height: 2rem;
    width: 2rem;
  }
`;

export const Label = styled.p`
  color: ${({ theme }) => theme.colors.lightest.hex()};
  font-size: ${({ theme }) => theme.sizes.p};
`;

export const Button = styled.button`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.primary.hex()};
  border: none;
  box-shadow: ${({ theme }) => theme.shadows.sm};
  display: flex;
  justify-content: center;
  min-height: 4.8rem;
  padding: 1rem 2rem;
  transition: background-color 400ms;
  will-change: background-color;
  :hover {
    background-color: ${({ theme }) => theme.colors.primaryDark.hex()};
  }

  &.--square {
    > ${Icon} {
      margin: 0;
    }
    height: 4.8rem;
    padding: 0;
    width: 4.8rem;
  }
`;
