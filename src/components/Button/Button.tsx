import React, { MouseEvent } from 'react';

import * as S from './styled';

type Props = {
  className?: string;
  Icon?: JSX.Element | null;
  label?: string;
  type?: 'button' | 'submit';
  onClick?: (e: MouseEvent) => void;
};
export default function Button({
  className = '',
  Icon = null,
  label = '',
  onClick,
  type = 'button',
}: Props): JSX.Element {
  return (
    <S.Button
      className={`${className} ${!label ? '--square' : ''}`}
      type={type}
      onClick={(e) => (onClick ? onClick(e as MouseEvent) : null)}
    >
      {Icon ? <S.Icon>{Icon}</S.Icon> : null}
      {label ? <S.Label>{label}</S.Label> : null}
    </S.Button>
  );
}
