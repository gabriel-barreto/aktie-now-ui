import React from 'react';
import styled, { css } from 'styled-components';

const ActiveIcon = css`
  &.--active {
    & {
      transform: rotate(45deg);
    }
    &::before {
      margin: 8px 0 0 0;
    }
    &::after {
      margin: 0 0 8px 0;
      transform: rotate(-90deg);
    }
  }
`;
const Icon = styled.span`
  position: relative;
  transition: transform 400ms;
  will-change: transform;

  &,
  &::before,
  &::after {
    background-color: ${({ theme }) => theme.colors.lightest.hex()};
    display: block;
    height: 0.48rem;
    width: 2.4rem;
  }

  &::before,
  &::after {
    content: '';
    position: absolute;
    transition: margin 400ms, transform 400ms;
    will-change: margin, transform;
  }

  &::before {
    top: -8px;
  }

  &::after {
    bottom: -8px;
  }

  ${ActiveIcon};
`;

type Props = {
  active: boolean;
};
export default function MenuTogglerIcon({ active }: Props): JSX.Element {
  return <Icon className={`${active ? '--active' : ''}`} />;
}
