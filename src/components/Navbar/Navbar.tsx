import React from 'react';

import Brand from '../../assets/aktie-books-logo.png';

import { useSidebarContext } from '../../contexts';

import TogglerIcon from './TogglerIcon';
import * as S from './styled';

type Props = {
  title: string;
};
export default function Navbar({ title }: Props): JSX.Element {
  const appName = 'Aktie Books';
  const { state, setState } = useSidebarContext();

  function onToggleHandler() {
    if (setState) setState(!state);
  }

  return (
    <S.Navbar>
      <S.Item>
        <S.Toggler type="button" onClick={onToggleHandler}>
          <TogglerIcon active={Boolean(state)} />
        </S.Toggler>
        <S.Brand title={appName} alt={appName} src={Brand} />
      </S.Item>
      <S.Item>
        <S.Title>{title}</S.Title>
      </S.Item>
      <S.Item>
        <S.Nav>
          <S.NavItem>
            <S.NavLink to="/logout">Sair</S.NavLink>
          </S.NavItem>
        </S.Nav>
      </S.Item>
    </S.Navbar>
  );
}
