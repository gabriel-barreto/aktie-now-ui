import styled from 'styled-components';

import { Link } from 'react-router-dom';

export const Navbar = styled.nav`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.dark.hex()};
  column-gap: 1rem;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  left: 0;
  max-height: 9.6rem;
  padding: 2.4rem 3.2rem;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 99;
`;

export const Item = styled.div`
  align-items: center;
  display: flex;
  :nth-of-type(2) {
    justify-content: center;
  }
  :last-of-type {
    justify-content: flex-end;
  }
`;

export const Title = styled.h1`
  color: ${({ theme }) => theme.colors.lightest.hex()};
  font-size: ${({ theme }) => theme.sizes.h3};
  line-height: 128%;
  text-align: center;
  text-overflow: ellipsis;
  @media (min-width: 768px) {
    margin: 0 0 0 12rem;
  }
`;

export const Toggler = styled.button`
  align-items: center;
  background-color: transparent;
  border: none;
  display: flex;
  height: 4.8rem;
  justify-content: center;
  outline-color: ${({ theme }) => theme.colors.primary};
  width: 5.6rem;

  > svg {
    fill: ${({ theme }) => theme.colors.lightest};
    transition: fill 400ms;
    will-change: fill;
  }

  :hover > svg {
    fill: ${({ theme }) => theme.colors.primary};
  }

  @media (min-width: 768px) {
    display: none;
  }
`;

export const Brand = styled.img`
  height: auto;
  margin: 0 1rem 0 0;
  min-height: 4.8rem;
  max-height: 100%;
  max-width: 100%;
  object-fit: contain;
  object-position: left center;
  width: auto;

  @media (max-width: 767px) {
    display: none;
  }
`;

export const Nav = styled.ul`
  align-items: center;
  display: flex;
  justify-content: flex-end;
  height: 100%;
`;

export const NavItem = styled.li`
  height: 100%;
`;

export const NavLink = styled(Link)`
  color: ${({ theme }) => theme.colors.lightest.hex()};
  font-size: 1.6rem;
  padding: 1.6rem;
  transition: color 400ms;
  will-change: color;

  :hover {
    color: ${({ theme }) => theme.colors.primary.hex()};
  }
`;
