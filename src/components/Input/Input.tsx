import React, { useEffect, useRef } from 'react';
import { useField } from '@unform/core';

import * as S from './styled';

type Props = {
  className?: string;
  err?: string;
  helper?: string;
  label: string;
  name: string;
  placeholder?: string;
  required?: boolean;
  type?: string;
  value?: string;
};
export default function Input({
  className = '',
  err,
  helper,
  label,
  name,
  placeholder = '',
  required = false,
  type = 'text',
  value = '',
}: Props): JSX.Element {
  const inputRef = useRef(null);
  const {
    fieldName,
    defaultValue = value,
    registerField,
    error = err,
  } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
    });
  }, [fieldName, registerField]);

  return (
    <S.Input className={className}>
      <S.Label htmlFor={fieldName}>{`${label}${required ? '*' : ''}`}</S.Label>
      {helper ? <S.Helper>{helper}</S.Helper> : null}
      <S.Field
        className={error ? '--error' : ''}
        defaultValue={defaultValue}
        placeholder={placeholder}
        ref={inputRef}
        required={required}
        type={type}
      />
      {error ? <S.Error>{error}</S.Error> : null}
    </S.Input>
  );
}
