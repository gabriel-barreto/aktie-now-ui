import styled from 'styled-components';

export const Input = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Field = styled.input`
  background-color: ${({ theme }) => theme.colors.light.hex()};
  border: 1px solid ${({ theme }) => theme.colors.dark.alpha(0.16)};
  font-size: ${({ theme }) => theme.sizes.p};
  color: ${({ theme }) => theme.colors.darkest};
  outline-color: ${({ theme }) => theme.colors.primary.hex()};
  padding: 1.6rem;
  transition: border-color 400ms;
  will-change: border-color;

  ::placeholder {
    color: ${({ theme }) => theme.colors.dark.alpha(0.8)};
    transition: color 400ms;
    will-change: color;
  }

  &.--error {
    border-color: ${({ theme }) => theme.colors.danger.hex()};
    ::placeholder {
      color: ${({ theme }) => theme.colors.danger.hex()};
    }
  }
`;

export const Label = styled.label`
  color: ${({ theme }) => theme.colors.dark.alpha(0.8)};
  font-size: ${({ theme }) => theme.sizes.p};
  margin: 0 0 0.8rem 0;
`;

export const Helper = styled.p`
  color: ${({ theme }) => theme.colors.dark.hex()};
  font-size: ${({ theme }) => theme.sizes.light};
  margin: 0 0 0.8rem 0;
`;

export const Error = styled.p`
  color: ${({ theme }) => theme.colors.danger.hex()};
  font-size: ${({ theme }) => theme.sizes.p};
  margin: 0.8rem 0 0 0;
`;
