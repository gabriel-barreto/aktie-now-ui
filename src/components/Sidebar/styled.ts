import styled from 'styled-components';

import { Link } from 'react-router-dom';

export const Sidebar = styled.nav`
  background-color: ${({ theme }) => theme.colors.dark};
  box-shadow: 2px 0 8px ${({ theme }) => theme.colors.darkest.alpha(0.48)};
  height: 100%;
  padding: 9.6rem 0 0 0;
  position: fixed;
  left: 0;
  top: 0;
  transform: translate(-1000px, 0);
  transition: transform 400ms;
  width: 12rem;
  will-change: transform;

  &.--active {
    transform: translate(0, 0);
  }

  @media (min-width: 768px) {
    transform: translate(0, 0);
  }
`;

export const SidebarItem = styled(Link)`
  align-items: center;
  color: ${({ theme }) => theme.colors.lightest};
  display: flex;
  flex-direction: column;
  font-size: 1.6rem;
  min-height: 4.8rem;
  padding: 1.6rem;
  transition: color 400ms;
  width: 100%;
  will-change: color;

  > svg {
    fill: ${({ theme }) => theme.colors.lightest};
    margin: 0 0 0.8rem 0;
    transition: fill 400ms;
    will-change: fill;
  }

  :hover {
    color: ${({ theme }) => theme.colors.primary};
    > svg {
      fill: ${({ theme }) => theme.colors.primary};
    }
  }

  &.--active {
    color: ${({ theme }) => theme.colors.primaryLight.hex()};
    > svg {
      fill: ${({ theme }) => theme.colors.primaryLight.hex()};
    }
  }
`;
