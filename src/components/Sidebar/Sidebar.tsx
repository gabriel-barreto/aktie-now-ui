import React from 'react';
import { useLocation } from 'react-router-dom';

import { Book } from 'styled-icons/material';

import { useSidebarContext } from '../../contexts';
import * as S from './styled';

export default function Sidebar(): JSX.Element {
  const { state } = useSidebarContext();
  const { pathname } = useLocation();

  return (
    <S.Sidebar className={`${state ? '--active' : ''}`}>
      <S.SidebarItem
        to="/livros"
        className={pathname.includes('/livros') ? '--active' : ''}
      >
        <Book size={24} />
        Livros
      </S.SidebarItem>
    </S.Sidebar>
  );
}
