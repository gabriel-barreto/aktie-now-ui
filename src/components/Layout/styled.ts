import styled from 'styled-components';

import Button from '../Button';

export const Base = styled.div``;

export const Action = styled(Button)`
  @media (max-width: 767px) {
    height: 4.4rem;
    padding: 0;
    width: 4.4rem;
    > p {
      display: none;
    }
    > span {
      margin: 0;
    }
  }
`;

export const Actions = styled.div`
  align-items: center;
  display: flex;
  width: 100%;

  > ${Action}:not(:last-of-type) {
    margin: 0 0.6rem;
  }
`;

export const ActionsBar = styled.div`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.light.hex()};
  border-bottom: 1px solid ${({ theme }) => theme.colors.dark.alpha(0.16)};
  display: flex;
  justify-content: space-between;
  padding: 1.6rem 3.2rem;
  > ${Actions} {
    width: 100%;
    :last-of-type {
      justify-content: flex-end;
    }
  }
`;

export const Wrapper = styled.div`
  padding: 9.6rem 0 0 0;
  @media (min-width: 768px) {
    padding: 9.6rem 0 0 12rem;
  }
`;

export const Container = styled.main`
  padding: 2.4rem 3.2rem 3.2rem 3.2rem;
  @media (min-width: 768px) {
    padding: 2.4rem 3.2rem 3.2rem 2.4rem;
  }
`;
