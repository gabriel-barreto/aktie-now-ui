import React from 'react';

import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

import { useLoader } from '../../contexts';

import Loader from '../Loader';
import Navbar from '../Navbar';
import SEO from '../SEO';
import Sidebar from '../Sidebar';

import * as S from './styled';

type Action = {
  Icon?: JSX.Element | null;
  label: string;
  onClick: () => void;
};
type Props = {
  actions?: {
    left?: Action[];
    right?: Action[];
  };
  children?: JSX.Element;
  title: string;
};
export default function Layout({
  actions = { left: [], right: [] },
  children,
  title,
}: Props): JSX.Element {
  const { state: loaderState } = useLoader();

  const buildActions = (actionsArray: Action[] = []) => (
    <>
      {actionsArray
        ? actionsArray.map(({ Icon, label, onClick }) => (
            <S.Action key={label} onClick={onClick} Icon={Icon} label={label} />
          ))
        : null}
    </>
  );
  const LeftActions = () => buildActions(actions?.left);
  const RightActions = () => buildActions(actions?.right);

  return (
    <S.Base>
      <SEO title={title} />

      <Navbar title={title} />
      <Sidebar />

      <S.Wrapper>
        {actions.left || actions.right ? (
          <S.ActionsBar>
            <S.Actions>
              <LeftActions />
            </S.Actions>
            <S.Actions>
              <RightActions />
            </S.Actions>
          </S.ActionsBar>
        ) : null}
        <S.Container>{children}</S.Container>
      </S.Wrapper>

      <ToastContainer />
      <Loader state={loaderState} />
    </S.Base>
  );
}
