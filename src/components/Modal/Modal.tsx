import React from 'react';

import AttentionIllustration from '../../assets/attention-illustration.png';

import * as S from './styled';

type Props = {
  action?: {
    cancel: string;
    confirm: string;
  };
  message?: string;
  state?: boolean;
  title: string;
  onClose: () => void;
  onConfirm: () => void;
};
export default function Modal({
  action = { cancel: 'Cancelar', confirm: 'Ok!' },
  message = '',
  state = false,
  title,
  onClose,
  onConfirm,
}: Props): JSX.Element {
  return (
    <S.Modal className={state ? '--visible' : ''}>
      <S.Overlay onClick={onClose} />
      <S.Dialog>
        <S.Header>
          <S.Close onClick={onClose}>X</S.Close>
          <S.Icon src={AttentionIllustration} title="Atenção!" alt="Atenção!" />
          <S.Title>{title}</S.Title>
        </S.Header>
        <S.Body>
          <S.Content>{message}</S.Content>
        </S.Body>
        <S.Footer>
          <S.Cancel onClick={onClose}>{action.cancel}</S.Cancel>
          <S.Confirm onClick={onConfirm}>{action.confirm}</S.Confirm>
        </S.Footer>
      </S.Dialog>
    </S.Modal>
  );
}
