import styled from 'styled-components';

export const Icon = styled.img`
  display: block;
  height: auto;
  object-fit: contain;
  object-position: center;
  margin: 0 0 2.4rem 0;
  max-height: 16rem;
  max-width: 100%;
  width: auto;
`;

export const Title = styled.h2`
  color: ${({ theme }) => theme.colors.primary.hex()};
  font-size: ${({ theme }) => theme.sizes.h3};
  font-weight: ${({ theme }) => theme.weights.strong};
  line-height: 136%;
  text-align: center;
  text-transform: uppercase;
`;

export const Content = styled.p`
  color: ${({ theme }) => theme.colors.dark.hex()};
  font-size: ${({ theme }) => theme.sizes.p};
  line-height: 144%;
  text-align: center;
`;

export const Close = styled.button`
  align-items: center;
  background-color: transparent;
  border: none;
  color: ${({ theme }) => theme.colors.primary.hex()};
  display: flex;
  height: 4.8rem;
  justify-content: center;
  position: absolute;
  right: 8px;
  top: 8px;
  transition: color 400ms;
  width: 4.8rem;
  will-change: color;

  :hover {
    color: ${({ theme }) => theme.colors.primaryDark.hex()};
  }
`;

const ActionBase = styled.button`
  border: none;
  font-size: ${({ theme }) => theme.sizes.p};
  line-height: 128%;
  min-height: 4.8rem;
  padding: 1.6rem;
  text-align: center;
  transition: background-color 400ms, color 400ms;
  width: 100%;
  will-change: background-color, color;
`;
export const Confirm = styled(ActionBase)`
  background-color: ${({ theme }) => theme.colors.primary};
  color: ${({ theme }) => theme.colors.lightest.hex()};
  :hover {
    background-color: ${({ theme }) => theme.colors.primaryDark.hex()};
    color: ${({ theme }) => theme.colors.light.hex()};
  }
`;

export const Cancel = styled(ActionBase)`
  background-color: transparent;
  color: ${({ theme }) => theme.colors.primary.hex()};
  :hover {
    color: ${({ theme }) => theme.colors.primaryDark.hex()};
  }
`;

export const Overlay = styled.div`
  background-color: ${({ theme }) => theme.colors.dark.hex()};
  position: fixed;
  height: 100%;
  left: 0;
  top: 0;
  width: 100%;
  z-index: 999;
`;

export const Dialog = styled.section`
  background-color: ${({ theme }) => theme.colors.lightest.hex()};
  box-shadow: ${({ theme }) => theme.shadows.md};
  max-width: 48rem;
  width: 88%;
  z-index: 999;
`;

export const Header = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 3.2rem 3.2rem 0 3.2rem;
  position: relative;
`;

export const Body = styled.div`
  padding: 0 3.2rem;
  margin: 0 0 2.4rem 0;
`;

export const Footer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`;

export const Modal = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  justify-content: center;
  left: 0;
  opacity: 0;
  position: fixed;
  top: 0;
  transition: opacity 400ms, visiblity 400ms;
  visibility: hidden;
  width: 100%;
  will-change: opacity, visibility;
  z-index: 99;

  &.--visible {
    opacity: 1;
    visibility: visible;
  }
`;
