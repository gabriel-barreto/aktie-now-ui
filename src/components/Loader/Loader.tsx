import React from 'react';

import * as S from './styled';

type Props = {
  state?: boolean;
};
export default function Loader({ state = false }: Props): JSX.Element {
  return (
    <S.Overlay className={state ? '--visible' : ''}>
      <S.SpinnerContainer>
        <S.Spinner />
        <S.Label>Carregando...</S.Label>
      </S.SpinnerContainer>
    </S.Overlay>
  );
}
