import styled from 'styled-components';

export const Overlay = styled.div`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.dark.alpha(0.64)};
  background-blend-mode: multiply;
  display: flex;
  height: 100%;
  justify-content: center;
  left: 0;
  opacity: 0;
  position: fixed;
  top: 0;
  transition: opacity 400ms, visibility 400ms;
  visibility: hidden;
  width: 100%;
  will-change: opacity, visibility;
  z-index: 9999;

  &.--visible {
    opacity: 1;
    visibility: visible;
  }
`;

export const SpinnerContainer = styled.div`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.darkest.alpha(0.88)};
  box-shadow: 0 2px 8px ${({ theme }) => theme.colors.darkest.alpha(0.24)};
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 3.2rem 4rem;
`;

export const Label = styled.p`
  color: ${({ theme }) => theme.colors.lightest.hex()};
  font-size: 1.6rem;
  margin: 2.4rem 0 0 0;
  text-align: center;
`;

export const Spinner = styled.span`
  @keyframes spin {
    to {
      transform: rotate(360deg);
    }
  }

  animation: spin 880ms linear infinite;
  background-color: transparent;
  border: 16px solid ${({ theme }) => theme.colors.lightest.alpha(0.16)};
  border-bottom-color: ${({ theme }) => theme.colors.lightest.hex()};
  border-radius: 50%;
  display: block;
  height: 12rem;
  width: 12rem;
`;
