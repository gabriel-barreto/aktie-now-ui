import styled from 'styled-components';

export const Message = styled.div`
  align-items: center;
  background-color: ${({ theme }) => theme.colors.lightest.hex()};
  border-left: 8px solid ${({ theme }) => theme.colors.primary.hex()};
  box-shadow: 0 2px 8px ${({ theme }) => theme.colors.darkest.alpha(0.24)};
  display: flex;
  max-width: 40rem;
  padding: 3.2rem 0;
  width: 100%;
`;

export const Icon = styled.span`
  display: block;
  height: 3.2rem;
  margin: 0 1rem;
  width: 3.2rem;
  > svg {
    fill: ${({ theme }) => theme.colors.primary.hex()};
    height: 3.2rem;
    width: 3.2rem;
  }
`;

export const Content = styled.p`
  color: ${({ theme }) => theme.colors.dark.hex()};
  font-size: 1.6rem;
  line-height: 128%;
  margin: 0 1.6rem 0 0;
`;
