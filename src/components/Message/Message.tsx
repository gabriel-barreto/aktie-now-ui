import React from 'react';

import { AlertCircle } from 'styled-icons/evaicons-solid';

import * as S from './styled';

type Props = {
  Icon?: JSX.Element;
  content: string;
};
export default function Message({
  Icon = <AlertCircle size={32} />,
  content,
}: Props): JSX.Element {
  return (
    <S.Message>
      <S.Icon>{Icon}</S.Icon>
      <S.Content>{content}</S.Content>
    </S.Message>
  );
}
