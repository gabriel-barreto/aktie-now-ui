import React from 'react';
import { Helmet } from 'react-helmet';

import content from './content';

interface IMeta {
  content: string;
  name: string;
  property?: string;
}
interface ISEOParams {
  author?: string;
  description?: string;
  lang?: string;
  meta?: IMeta[];
  name?: string;
  title?: string;
}
export default function SEO({
  author = content.author,
  description = content.description,
  lang = content.lang,
  meta = content.meta,
  name = content.name,
  title = '',
}: ISEOParams = {}): JSX.Element {
  return (
    <Helmet
      htmlAttributes={{ lang }}
      title={title}
      titleTemplate={title ? `%s | ${name}` : name}
      meta={[
        {
          name: 'description',
          content: description,
        },
        {
          property: 'og:title',
          content: title,
        },
        {
          property: 'og:description',
          content: description,
        },
        {
          property: 'og:type',
          content: 'website',
        },
        {
          name: 'twitter:card',
          content: 'summary',
        },
        {
          name: 'twitter:creator',
          content: author,
        },
        {
          name: 'twitter:title',
          content: title,
        },
        {
          name: 'twitter:description',
          content: description,
        },
        ...meta,
      ]}
    />
  );
}
