import { FormHandles } from '@unform/core';
import { RefObject } from 'react';
import { array, object, string, ValidationError } from 'yup';

import { Book, BookDetails, BookFormData, BookFormRawData } from '../types';

import $http from './http.service';

const path = '/books';

async function handleFormSubmit(
  { authors, categories, ...rest }: BookFormRawData,
  formRef: RefObject<FormHandles>,
): Promise<BookFormData> {
  const schema = object().shape({
    title: string().required('Um título é necessário para registrar um livro.'),
    authors: array()
      .of(string())
      .min(1, 'Ao menos 1 autor deve ser informado.'),
    categories: array()
      .of(string())
      .min(1, 'Ao menos 1 gênero deve ser informado.'),
    publisher: string().required(
      'Uma editora é necessária para registrar um livro.',
    ),
  });

  try {
    const payload = {
      ...rest,
      authors: authors.split(',').map((author) => author.trim()),
      categories: categories.split(',').map((tag) => tag.trim()),
    };

    await schema.validate(payload, { abortEarly: false });

    return payload;
  } catch (ex) {
    if (ex instanceof ValidationError) {
      const validationErrors = Object.fromEntries(
        ex.inner.map((err) => [err.path, err.message]),
      );
      formRef.current?.setErrors(validationErrors);
    }

    return Promise.reject(ex);
  }
}

async function create(payload: BookFormData): Promise<Book> {
  try {
    const res = await $http.post(path, payload);
    return res.data;
  } catch (ex) {
    return Promise.reject(ex);
  }
}

async function fetchAll(): Promise<Book[]> {
  try {
    const res = await $http.get(path);
    return res.data;
  } catch (ex) {
    return Promise.reject(ex);
  }
}

async function fetchOne(_id: string): Promise<BookDetails> {
  try {
    const res = await $http.get(`${path}/${_id}`);
    return res.data;
  } catch (ex) {
    return Promise.reject(ex);
  }
}

async function remove(_id: string): Promise<boolean> {
  try {
    await $http.delete(`${path}/${_id}`);
    return true;
  } catch (ex) {
    return Promise.reject(ex);
  }
}

async function update(
  _id: string,
  payload: BookFormData,
): Promise<BookDetails> {
  try {
    const res = await $http.put(`${path}/${_id}`, payload);
    return res.data;
  } catch (ex) {
    return Promise.reject(ex);
  }
}

export default { create, fetchAll, fetchOne, handleFormSubmit, remove, update };
