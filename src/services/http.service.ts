import axios from 'axios';
import PubSub from 'pubsub-js';

import { API as APIConfig } from '../config';
import { LoaderContextTopic } from '../contexts/loader.context';

const $http = axios.create(APIConfig);

$http.interceptors.request.use(
  (config) => {
    PubSub.publish(LoaderContextTopic, true);
    return config;
  },
  (err) => {
    PubSub.publish(LoaderContextTopic, false);
    return Promise.reject(err);
  },
);

$http.interceptors.response.use(
  (response) => {
    PubSub.publish(LoaderContextTopic, false);
    return response;
  },
  (err) => {
    PubSub.publish(LoaderContextTopic, false);
    return Promise.reject(err);
  },
);

export default $http;
