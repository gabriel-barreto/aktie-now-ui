import { css } from 'styled-components';

export default css`
  .Toastify {
    font-size: 1.6rem;

    .Toastify__toast {
      padding: 2.4rem 1.6rem;
    }

    .Toastify__toast-body {
      line-height: 120%;
    }
  }
`;
