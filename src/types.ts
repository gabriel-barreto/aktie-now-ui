type BookBase = {
  title: string;
  publisher: string;
};

type BookCommon = {
  _id: string;
  title: string;
  rating: number;
  updatedAt: string;
};

export type BookFormRawData<T = BookBase> = {
  [P in keyof T]: T[P];
} & {
  authors: string;
  categories: string;
};

export type BookFormData<T = BookBase> = {
  [P in keyof T]: T[P];
} & {
  authors: string[];
  categories: string[];
};

export type Book<T = BookCommon> = { [P in keyof T]: T[P] } & {
  authors: string;
};

export type BookDetails<T = BookCommon> = { [P in keyof T]: T[P] } & {
  authors: string[];
  categories: string[];
  publisher: string;
  createdAt: string;
};
