import PubSub from 'pubsub-js';
import React, { createContext, useContext, useEffect, useState } from 'react';

const INITIAL_STATE = false;

type LoaderContextType = {
  state: boolean;
  setState: (state: boolean) => void;
};
const LoaderContext = createContext<LoaderContextType>({
  state: INITIAL_STATE,
  setState: (_) => null,
});

export const LoaderContextTopic = 'LOADER_CONTEXT';

type Props = {
  children: JSX.Element | null;
};
export function LoaderContextProvider({ children }: Props): JSX.Element {
  const [state, setState] = useState(INITIAL_STATE);
  const subscriber = (_: string, data: boolean) => setState(data);

  useEffect(() => {
    PubSub.subscribe(LoaderContextTopic, subscriber);
  }, []);
  return (
    <LoaderContext.Provider value={{ state, setState }}>
      {children}
    </LoaderContext.Provider>
  );
}

export function useLoader(): LoaderContextType {
  return useContext(LoaderContext);
}
