import React from 'react';

import { SidebarContextProvider, useSidebarContext } from './sidebar.context';
import { LoaderContextProvider, useLoader } from './loader.context';

type Props = {
  children: JSX.Element | null;
};
export default function AppContextProvider({ children }: Props): JSX.Element {
  return (
    <LoaderContextProvider>
      <SidebarContextProvider>{children}</SidebarContextProvider>
    </LoaderContextProvider>
  );
}

// ==> Hooks
export { useLoader, useSidebarContext };
