import React, { createContext, useContext, useState } from 'react';

type SidebarContextType = {
  state?: boolean;
  setState?: (state: boolean) => void;
};
const SidebarContext = createContext<SidebarContextType>({});

type Props = {
  children: JSX.Element | null;
};
export function SidebarContextProvider({ children }: Props): JSX.Element {
  const [state, setState] = useState(false);

  return (
    <SidebarContext.Provider value={{ state, setState }}>
      {children}
    </SidebarContext.Provider>
  );
}

export function useSidebarContext(): SidebarContextType {
  return useContext(SidebarContext);
}
