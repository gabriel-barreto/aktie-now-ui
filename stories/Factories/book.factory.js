import faker from 'faker';

export function one() {
  return {
    _id: faker.random.uuid(),
    title: faker.lorem.words(),
    authors: Array.from(
      { length: faker.random.number({ min: 1, max: 2 }) },
      faker.name.findName,
    ).join(', '),
    rating: faker.random.number({ min: 1, max: 5 }),
    updatedAt: faker.date.past().toLocaleDateString(),
  };
}

export function list(seeds = 5) {
  return Array.from({ length: seeds }, one);
}
