import styled from 'styled-components';
import { ReactLogo } from 'styled-icons/boxicons-logos';

export const CustomIcon = styled(ReactLogo)`
  @keyframes rotate {
    to {
      transform: rotate(360deg);
    }
  }

  animation: rotate 2s linear infinite;
`;
