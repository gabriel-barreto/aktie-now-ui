import styled from 'styled-components';

export const Base = styled.div`
  background-color: ${({ theme }) => theme.colors.lightest.hex()};
  min-height: 40rem;
  padding: 3.2rem;
  width: 100%;
`;
