import styled from 'styled-components';

export const LightBackground = styled.div`
  background-color: #fff;
  padding: 3.2rem;
  width: 100%;
`;
